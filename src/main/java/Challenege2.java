
public class Challenege2 {
	
	public static void main (String []arg) {
		String input = ")())))())))())))())))())))())))())))())))())))())))())))()))";
		String patternToRemove = ")(";
		System.out.println("parenthesisAnnihilator1:" + parenthesisAnnihilator1(input, patternToRemove));
		System.out.println("***********************************************************");
		System.out.println("parenthesisAnnihilator2:" + parenthesisAnnihilator2(input, patternToRemove));
	}
	
	
	/**
	 * 
	 * @param input
	 * @param patternToRemove
	 * @return
	 */
	public static String parenthesisAnnihilator1 (String input, String patternToRemove) {
		long start = System.currentTimeMillis();
		
		if (input != null && patternToRemove != null) {
			while ( input.length() > 0 && input.indexOf(patternToRemove) >= 0) {
				input = input.replace(patternToRemove, "");
			}			
		}
		System.out.println("parenthesisAnnihilator1 took: " + (System.currentTimeMillis() - start) + " milli seconds.");
		return input;
	}
	
	
	/**
	 * 
	 * @param input
	 * @param patternToRemove
	 * @return
	 */
	public static String parenthesisAnnihilator2 (String input, String patternToRemove) {
		long start = System.currentTimeMillis();
		
		if (input != null && patternToRemove != null) {
			StringBuilder result = new StringBuilder(input);
			int index = result.indexOf(patternToRemove);
			int lengthOfPatternToRemove = patternToRemove.length();
			
			while (result.length() > 0 && index >= 0) {
					result.replace(index, index + lengthOfPatternToRemove, "");
					index = result.indexOf(patternToRemove);
			}
			
			System.out.println("parenthesisAnnihilator2 took: " + (System.currentTimeMillis() - start) + " milli seconds.");
			return result.toString();
		}
		
		return "You passed a null String.";
	}
}
