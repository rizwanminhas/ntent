
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


public class Challenge1 {	

	public static void main (String args[]) throws IOException {
		Set<String> ignoredWords = new HashSet<String>();
		
		try (Scanner scanner = new Scanner(new File("stopwords.txt"))) {
			while (scanner.hasNext()) {
				ignoredWords.add(scanner.next().toLowerCase());
			}	
		}

		Document doc = Jsoup.connect("http://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml").get();
		Elements elems = doc.select("item > description");
		Map<String, Integer> wordsCount = new HashMap<String, Integer>();
		
		int ignoredWordsCount = 0;
		
		for (Element elem : elems) {
			String[] wordArray = Jsoup.parse(elem.text()).text().toLowerCase().split(" ");
			for (int i=0; i<wordArray.length; i++) {
				if (!ignoredWords.contains(wordArray[i])) {
					if (wordsCount.containsKey(wordArray[i])) {
						wordsCount.put(wordArray[i], wordsCount.get(wordArray[i])+1);
					}
					else {
						wordsCount.put(wordArray[i], 1);
					}
				}
				else {
					ignoredWordsCount ++;
				}
			}	
		}
		
		ValueComparator vc = new ValueComparator(wordsCount);
		TreeMap<String, Integer> sortedWordsCount = new TreeMap<String, Integer>(vc);
		sortedWordsCount.putAll(wordsCount);
		
		ObjectMapper om = new ObjectMapper();
		
		Result result = new Result(new ArrayList<Entry<String, Integer>>(), ignoredWordsCount);
		
		int topTen = 0;
		for (Entry<String, Integer> s : sortedWordsCount.entrySet()) {
			if (topTen == 10)
				break;
			result.getWords().add(s);
			topTen ++;
		}
		
		om.enable(SerializationFeature.INDENT_OUTPUT);
		System.out.println(om.writeValueAsString(result));
	}
	
	
	
}


/**
 * Sort a map by value in descending order
 * 
 * @author rizwan.minhas
 *
 */
class ValueComparator implements Comparator<String> {
	private Map<String, Integer> map;
	
	public ValueComparator(Map<String, Integer> map) {
		this.map = map;
	}
	public int compare(String o1, String o2) {
		if (map.get(o1) < map.get(o2))
			return 1;
		return -1;
	}	
}


/**
 * Pojo representing Json object
 * 
 * @author rizwan.minhas
 *
 */
class Result {
	private int stopWordsIgnored;
	@JsonSerialize(using = WordSerialzer.class)
	private List<Entry<String, Integer>> words;
	
	public Result(List<Entry<String, Integer>> words, int stopWordsIgnored) {
		this.words = words;
		this.stopWordsIgnored = stopWordsIgnored;
	}

	public int getStopWordsIgnored() {
		return stopWordsIgnored;
	}

	public void setStopWordsIgnored(int stopWordsIgnored) {
		this.stopWordsIgnored = stopWordsIgnored;
	}

	public List<Entry<String, Integer>> getWords() {
		return words;
	}

	public void setWords(List<Entry<String, Integer>> words) {
		this.words = words;
	}
}


/**
 * Json serializer to convert the map's content to "word" and "count" instead of "key" and "value"
 * 
 * @author rizwan.minhas
 *
 */
class WordSerialzer extends JsonSerializer<List<Entry<String, Integer>>> {

	@Override
	public void serialize(List<Entry<String, Integer>> list, JsonGenerator jgen, SerializerProvider provider) throws IOException,	JsonProcessingException {
		jgen.writeStartArray();
		for (Entry<String, Integer> entry : list) {
			jgen.writeStartObject();
			jgen.writeStringField("word", entry.getKey());
			jgen.writeNumberField("count", entry.getValue());
			jgen.writeEndObject();
		}
		jgen.writeEndArray();
	}
}
